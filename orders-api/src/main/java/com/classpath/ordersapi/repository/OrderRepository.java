package com.classpath.ordersapi.repository;

import com.classpath.ordersapi.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    /*private Set<Order> orders = new HashSet<>(Arrays.asList(
      Order.builder().orderId(12).customerName("Harish").price(25_000).build(),
      Order.builder().orderId(13).customerName("Harsh").price(35_000).build(),
      Order.builder().orderId(15).customerName("Vishnu").price(55_000).build()
    ));

    public Set<Order> fetchOrders() {
        return this.orders;
    }

    public Optional<Order> fetchOrdersById(long orderId) {
        return this.orders
                    .stream()
                    .filter(order -> order.getOrderId() == orderId)
                    .findAny();
    }
    public Order saveOrder(Order order){
        this.orders.add(order);
        return order;
    }*/
}