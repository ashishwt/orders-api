package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Service
public class OrderService {

    private OrderRepository orderRepository;

    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order fetchOrdersById(long orderId) {
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException(" Invalid Order Id"));
    }

    public Order saveOrder(Order order){
        return this.orderRepository.save(order);
    }
}