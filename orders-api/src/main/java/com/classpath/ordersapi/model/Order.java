package com.classpath.ordersapi.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Column(name="customer_name", nullable = false, unique = true)
    @NotEmpty(message = "Customer name cannot be empty")
    private String customerName;

    @Max(value = 50000, message = "Order price is more than configured")
    @Min(value = 10000, message = "Order price should be more than 10K")
    private double price;

}